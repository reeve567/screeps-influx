import { Screeps } from "./screeps";
import sleep from "sleep-promise";
import "dotenv/config"

const screeps = new Screeps();

await screeps.run();