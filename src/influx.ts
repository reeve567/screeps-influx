import { InfluxDB, Point, WriteApi } from "@influxdata/influxdb-client";
import { HeapStats, StatsMemory } from "./types";

export class Influx {
	private client: InfluxDB;
	private writeClient: WriteApi;

	constructor() {
		const token = process.env.INFLUXDB_TOKEN || "";
		const url = process.env.INFLUXDB_URL || "";
		const org = "primary";
		const bucket = "screeps";


		this.client = new InfluxDB({ url, token });
		this.writeClient = this.client.getWriteApi(org, bucket, "ms");
	}

	public pushStats(data: StatsMemory, username: string) {
		this.writeClient.writePoint(
		  new Point("system")
			.timestamp(data.time)
			.floatField("used_cpu", data.system.cpuTime)
		    .tag("username", username)
		);

		this.writeClient.writePoint(
		  new Point("user")
		    .timestamp(data.time)
		    .uintField("gcl", data.user.gcl)
		    .uintField("maxCPU", data.user.maxCPU)
		    .floatField("credits", data.user.credits)
		    .uintField("pixels", data.user.pixels)
		    .uintField("bucket", data.user.bucket)
		    .tag("username", username)
		)

		data.rooms.forEach(room => {
			this.writeClient.writePoint(
			  new Point("rooms")
				.timestamp(data.time)
				.intField("creeps", room.creeps)
				.tag("room", room.name)
			    .tag("safeMode", room.safeMode.toString())
			    .tag("username", username)
			);

			for (let rolesKey in room.roles) {
				this.writeClient.writePoint(
				  new Point("roles")
					.timestamp(data.time)
					.tag("room", room.name)
					.tag("role", rolesKey)
					.intField("amount", room.roles[rolesKey])
				    .tag("username", username)
				);
			}
		});
	}

	public pushHeapStats(data: HeapStats, username: string) {
		this.writeClient.writePoint(
		  new Point("heap")
		    .timestamp(data.time)
		    .uintField("total_heap_size", data.heap.total_heap_size)
		    .uintField("total_heap_size_executable", data.heap.total_heap_size_executable)
		    .uintField("total_physical_size", data.heap.total_physical_size)
		    .uintField("total_available_size", data.heap.total_available_size)
		    .uintField("used_heap_size", data.heap.used_heap_size)
		    .uintField("heap_size_limit", data.heap.heap_size_limit)
		    .uintField("malloced_memory", data.heap.malloced_memory)
		    .uintField("peak_malloced_memory", data.heap.peak_malloced_memory)
		    .booleanField("does_zap_garbage", data.heap.does_zap_garbage == 1)
		    .uintField("externally_allocated_size", data.heap.externally_allocated_size)
		    .tag("username", username)
		)
	}

	public flush() {
		this.writeClient.flush();
	}
}