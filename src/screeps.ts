import { ScreepsAPI } from "screeps-api";
import { Influx } from "./influx";
import sleep from "sleep-promise";
import { HeapStats, StatsMemory } from "./types.ts";

export class Screeps {
	private readonly screeps: ScreepsAPI;
	private readonly influx: Influx = new Influx();
	private readonly statsSegment = 50;
	private readonly heapSegment = 51;
	private shard = process.env.SCREEPS_SHARD;
	private username = "";

	constructor() {
		const token = process.env.SCREEPS_TOKEN;

		this.screeps = new ScreepsAPI({
			token,
			protocol: "https",
			hostname: "screeps.com",
			port: 443,
			path: "/"
		});
	}

	async run() {
		console.log("Starting...");
		this.username = (await this.screeps.me()).username

		console.log(`Set the username to ${this.username}`);

		while (true) {
			const doHeap = await this.stats();
			if (doHeap) {
				await this.heap();
			}

			await sleep(1000 * 60 * 4);
		}
	}

	async stats(): Promise<boolean> {
		let data = await this.screeps.memory.segment.get(this.statsSegment, this.shard);
		this.screeps.memory.segment.set(this.statsSegment, "", this.shard);

		if (data.data.length > 0) {
			let parsed = JSON.parse(data.data) as StatsMemory[];
			console.log(`Got ${parsed.length} data points`);

			for (let stat of parsed) {
				this.influx.pushStats(stat, this.username);
			}

			return true;
		} else {
			// @ts-ignore
			const shard = Object.keys((await this.screeps.me()).cpuShard)[0];

			if (shard != undefined) {
				console.log(`Got ${shard} shard`);
				this.shard = shard;
			}

			return false;
		}
	}

	async heap() {
		let data = await this.screeps.memory.segment.get(this.heapSegment, this.shard);
		this.screeps.memory.segment.set(this.heapSegment, "", this.shard);

		if (data.data.length > 0) {
			let parsed = JSON.parse(data.data) as HeapStats[];
			console.log(`Got ${parsed.length} heap data points`);
			for (let stat of parsed) {
				this.influx.pushHeapStats(stat, this.username);
			}
		}
	}
}