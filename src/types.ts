export interface RoomStats {
	creeps: number;
	name: string;
	safeMode: boolean;
	roles: {
		[key: string]: number;
	}
}

export interface UserStats {
	gcl: number;
	maxCPU: number;
	credits: number;
	pixels: number;
	bucket: number;
}

export interface SystemStats {
	cpuTime: number;
}

export interface StatsMemory {
	system: SystemStats;
	user: UserStats;
	rooms: RoomStats[];
	time: number;
}

export interface HeapStats {
	time: number;
	heap: HeapStatistics
}